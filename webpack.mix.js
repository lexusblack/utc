const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 // mix.js('resources/js/app.js', 'public/js').vue().version();
 // mix.sass('resources/sass/app.scss', 'public/css').version();

// mix.js('resources/js/app.js', 'public/js')
//     .vue()
//     // .sass('resources/sass/app.scss', 'public/css', [
//     //     require("tailwindcss"),
//     //    ]);
//     .postCss("resources/css/app.css", "public/css", [
//         require("tailwindcss"),
//        ]);

mix.js('resources/js/app.js', 'public/js').vue().version();
mix.sass('resources/sass/app.scss', 'public/css').version();
mix.copy('node_modules/font-awesome/fonts/*', 'public/fonts');

