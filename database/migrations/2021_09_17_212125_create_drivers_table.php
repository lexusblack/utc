<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('secondary_name');
            $table->string('phone');
            $table->string('bill');
            $table->string('email');
            $table->string('password');
            $table->string('front_drive_card')->nullable();
            $table->string('back_drive_card')->nullable();
            $table->string('certificate')->nullable();
            $table->string('passport')->nullable();
            $table->string('photo')->nullable();
            $table->integer('car_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
