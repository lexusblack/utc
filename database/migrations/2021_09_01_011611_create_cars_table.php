<?php

use App\Models\Car;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->integer('mark_id');
            $table->integer('model_id');
            $table->date('year');
            $table->string('color');
            $table->string('number_car');
            $table->integer('fuel_id');
            $table->integer('transmission_id');
            $table->integer('price')->default(0);
            $table->date('date_technical_inspection');
            $table->string('technical_inspection_first')->nullable();
            $table->string('technical_inspection_second')->nullable();
            $table->date('date_insurance');
            $table->string('insurance_photo')->nullable();
            $table->integer('partner_id')->nullable();
            $table->string('status')->default(Car::STATUS_READY['value']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
