import Vue from 'vue';
import Router from 'vue-router';

// Компоненты
// import Login from '../views/Login';
import NotFound from '../components/NotFound';
import Base from '../components/Base';
import Dashboard from '../components/Dashboard';
import Driver from '../components/Driver';
import Car from '../components/Car';
import Partner from '../components/Partner';
import Blacklist from '../components/Blacklist';
import Message from '../components/Message';
import Mark from '../components/Mark';
import ModelCar from '../components/ModelCar';
import Fuel from '../components/Fuel';
import Transmission from '../components/Transmission';

import Register from '../components/pages/Register';


Vue.use(Router);

export default new Router({
    base: '/',
    linkActiveClass: 'active',
    mode: 'history',
    routes: [
        // {
        //     path: '/login',
        //     name: 'Login',
        //     component: Login
        // },
        {
            path: '/',
            name: 'Base',
            redirect: {name: 'Dashboard'},
            component: Base,
            children: [
                { path: '404', from: '*', redirect: {name: '404'} },
                { path: 'dashboard', name: 'Dashboard', component: Dashboard, meta: {title: "Общие данные", menuTitle: 'Dashboard', icon: 'fa-heartbeat', inMenu: true} },
                { path: 'driver', name: 'Driver', component: Driver, meta: {title: "Данные о водителях", menuTitle: 'Driver', icon:'fa-users', inMenu: true} },
                { path: 'car', name: 'Car', component: Car, meta: {title: "Данные о машинах", menuTitle: 'Car', icon: 'fa-car', inMenu: true} },
                { path: 'partner', name: 'Partner', component: Partner, meta: {title: "Данные о партнерах", menuTitle: 'Partner', icon: 'fa-user-secret', inMenu: true} },
                { path: 'blacklist', name: 'Blacklist', component: Blacklist, meta: {title: "Данные о мошенниках", menuTitle: 'BlackList', icon: 'fa-th-list', inMenu: true} },
                { path: 'message', name: 'Message', component: Message, meta: {title: "Сообщения", menuTitle: 'Message', icon: 'fa-commenting', inMenu: true} },

                { path: 'mark', name: 'Mark', component: Mark, meta: {title: "Марки авто", menuTitle: 'Marks', icon: 'fa-car', inMenu: true} },
                { path: 'model-car', name: 'ModelCar', component: ModelCar, meta: {title: "Модели авто", menuTitle: 'Model', icon: 'fa-car', inMenu: true} },
                { path: 'fuel', name: 'Fuel', component: Fuel, meta: {title: "Топливо авто", menuTitle: 'Fuel', icon: 'fa-car', inMenu: true} },
                { path: 'transmission', name: 'Transmission', component: Transmission, meta: {title: "Коробка передач авто", menuTitle: 'Transmission', icon: 'fa-car', inMenu: true} },
                // { path: 'driver', name: 'Driver', component: Driver, meta: {title: "Коробка передач авто", menuTitle: 'Transmission', icon: 'fa-car', inMenu: true} },
            ]
        },
        { path: '/register', name: 'Register', component: Register, meta: {title: 'Add'} },
        {
            path: '/404',
            name: '404',
            component: NotFound,
            meta: {inMenu: false}
        },
        {
            path: '/*',
            redirect: {name: '404'},
            meta: {inMenu: false}
        }
    ]
});
