export default {
    data() {
        return {
            model: {},
            formType: 'new',
            hidenForm: true,
        }
    },
    computed: {
        errors() {
            return this.$store.getters[this.$options.name.toLowerCase() + '/getErrors'];
        }
    },
    methods: {
        showForm() {
            this.hidenForm = false;
            this.formType = 'new';
        },
        hideForm() {
            this.hidenForm = true;
            this.clearModel();
            this.formType = 'update';
        },
        submit(response) {
            this.clearModel();
            this.hideForm();
        },
        clearModel() {
            for (let item in this.model) {
                if (this.model.hasOwnProperty(item)) {
                    this.model[item] = null;
                }
            }
        },
        changeFormType(type) {
            this.formType = type;
        }
    }
}
