window._ = require('lodash');

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

import Vue from 'vue';
import router from './router';
import store from './store';
import axios from 'axios';
import constants from './constants';
// import Vuex from 'vuex';



// Vue.http.headers.common['Access-Control-Allow-Origin'] = true;
// Vue.http.options.emulateJSON = true;
// axios.defaults.withCredentials = true;
// axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common.Accept = constants.contentType;
axios.defaults.baseURL = constants.baseUrl;
axios.defaults.timeout = constants.requestTimeout;


window.Vue = Vue;
window.router = router;
window.store = store;
window.axios = axios;

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });
