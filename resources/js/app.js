// window.Vue = require('vue').default;

// require('./bootstrap');

// import Vue from 'vue';
// import VueRouter from 'vue-router';

// import LoadScript from 'vue-plugin-load-script';

// Vue.use(LoadScript);

// Vue.use(VueRouter);

// Vue.component('main-component', require('./MainIndex.vue').default);
// import "tailwindcss/tailwind.css";

// // Vue.loadScript('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js');



// const app = new Vue({
//     el: '#app',
// });

require('./bootstrap');

// Vue.component('AddBlock', require('./components/AddBlock.vue').default);

const app = new Vue({
    el: '#app',
    router: window.router,
    store: window.store,
    template: '<router-view />'
});

