import Vue from 'vue';
import Vuex from 'vuex';

import actions from './actions';
import mutations from './mutations';

import alert from './modules/alert';
// import auth from './modules/auth';

import user from './modules/user';
import partner from './modules/partner';
import car from './modules/car';
import mark from './modules/mark';
import modelcar from './modules/modelCar';
import fuel from './modules/fuel';
import transmission from './modules/transmission';
import driver from './modules/driver';
import register from './modules/register'



Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        loading: false,
        ready: false
    },
    actions,
    mutations,
    modules: {
        alert,
        // auth,
        user,
        partner,
        car,
        mark,
        modelcar,
        fuel,
        transmission,
        driver,
        register
    },
    strict: process.env.NODE_ENV !== 'production'
});
