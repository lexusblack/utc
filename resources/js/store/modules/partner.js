import axios from 'axios';

export default {
    namespaced: true,
    state: {
        name: null,
        items: [],
        errors: [],
        paginate: {
            urls: [],
            total: 0,
            page: 1,
            lastPage: 0
        }
    },
    getters: {
        getName(state) {
            return state.name;
        },
        getItems(state) {
            return state.items;
        },
        getTotal(state) {
            return state.paginate.total;
        },
        getLastPage(state) {
            return state.paginate.lastPage;
        },
        getUrls(state) {
            return state.paginate.urls;
        },
        getErrors(state) {
            return state.errors;
        }
    },
    mutations: {
        ADD_ITTEM(state, item) {
            state.items.push(item);
        },
        FILL(state, response) {
            state.items = response.data;
            state.paginate.total = response.total;
            state.paginate.urls = response.links;
            state.paginate.lastPage = response.last_page;
        },
        SET_ERROR(state, error) {
            if (Array.isArray(error)) {
                state.errors = [];
            }
            else {
                state.errors.push(error);
            }
        }
    },
    actions: {
        fetch({commit, state}, data) {
            store.commit('LOADING', true);
            axios.get('/partner', {
                params: data
            }).then(response => {
                commit('FILL', response.data);
            }).finally(() => {
                store.commit('LOADING', false);
            });
        },
        save({commit, state, dispatch}, {data, config}) {
            store.commit('LOADING', true);

            data = Object.assign({}, state.paginate, data);
            let request_config = {};
            let formData = data;
            if (config.needFile || false) {
                request_config = {headers: {'Content-Type': 'multipart/form-data'}};

                formData = new FormData();
                for (let field in data) {
                    if (!data.hasOwnProperty(field) || data[field] === null) { continue; }
                    formData.append(field, data[field]);
                }
            }

            return axios.post('/partner/save', formData, request_config)
                .then(response => {
                    store.dispatch('alert/success', response.data.message);
                    commit('FILL', response.data.items);
                    // setTimeout(() => commit('SET_MESSAGE', null), 5000);
                    return Promise.resolve(response);
                }).finally(() => {
                    store.commit('LOADING', false);
                })
                .catch(error => {
                    store.dispatch('handle', error, {root: true});
                    return Promise.reject(error);
                })
        },
        delete({commit, state}, data) {
            store.commit('LOADING', true);
            axios.post('/partner/delete', Object.assign({}, data, state.paginate))
                .then(response => {
                    store.dispatch('alert/success', response.data.message);
                    commit('FILL', response.data.items);
                    // setTimeout(() => commit('SET_MESSAGE', null), 5000);
                }).finally(() => {
                store.commit('LOADING', false);
            })
                .catch(error => {
                    console.error(error);
                })
        }
    }
};
