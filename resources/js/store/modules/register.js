import axios from 'axios';

export default {
    namespaced: true,
    state: {
        first_name: null,
        secondary_name: null,
        phone: null,
        bill: null,
        email: null,
        password: null,
        front_drive_card: null,
        back_drive_card: null,
        certificate: null,
        photo: null,
        passport: null,
    },
    getters: {
        getFirstName(state) {
            return state.first_name;
        },
        getSecondName(state) {
            return state.secondary_name;
        },
        getPhone(state) {
            return state.phone;
        },
        getBill(state) {
            return state.bill;
        },
        getEmail(state) {
            return state.email;
        },
        getPassword(state) {
            return state.password;
        },
        getDriveCardFront(state) {
            return state.front_drive_card;
        },
        getDriveCardBack(state) {
            return state.back_drive_card;
        },
        getPhotoUnjail(state) {
            return state.certificate;
        },
        getPhoto(state) {
            return state.photo;
        },
        getPassportPhoto(state) {
            return state.passport
        },
        getAll(state) {
            return Object.assign({}, state);
        }
    },
    mutations: {
        SET_FIRST_NAME(state, item) {
            state.first_name = item;
        },
        SET_SECOND_NAME(state, item) {
            state.secondary_name = item;
        },
        SET_PHONE(state, item) {
            state.phone = item;
        },
        SET_BILL(state, item) {
            state.bill = item;
        },
        SET_EMAIL(state, item) {
            state.email = item;
        },
        SET_PASSWORD(state, item) {
            state.password = item;
        },
        SET_DRIVE_CARD_FRONT(state, item) {
            state.front_drive_card = item;
        },
        SET_DRIVE_CARD_BACK(state, item) {
            state.back_drive_card = item;
        },
        SET_PHOTO_UNJAIL(state, item) {
            state.certificate = item;
        },
        SET_PHOTO(state, item) {
            state.photo = item;
        },
        SET_PASSPORT_PHOTO(state, item) {
            state.passport = item;
        }
    },
    actions: {

        save({state}) {
            console.log(state);

            let formData = new FormData();
            for (let field in state) {
                if (!state.hasOwnProperty(field) || state[field] === null) {
                    continue;
                }
                formData.append(field, state[field]);
            }

            let request_config = {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                processData: false,
                contentType: false,
                mimeType: "multipart/form-data"
            };

            return axios.post('/driver/save', formData, request_config)
                .then(response => {
                    return Promise.resolve(response);
                })
                .catch(error => {
                    console.error(error);
                })

        },
        checkUnique({commit}, data) {
            return axios.get('/driver/' + data.field +'/' + data.value).then(response => {
                console.log('Check', response);
                return response.data;
            });
        }
    }
};