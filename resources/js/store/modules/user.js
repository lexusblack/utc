import axios from 'axios';

export default {
    namespaced: true,
    state: {
        name: 'Test'
    },
    getters: {
        getName(state) {
            return state.name;
        }
    },
    mutations: {},
    actions: {}
};
