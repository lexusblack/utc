<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PartnerController;
use App\Http\Controllers\Api\CarController;
use App\Http\Controllers\Api\MarkController;
use App\Http\Controllers\Api\ModelCarController;
use App\Http\Controllers\Api\FuelController;
use App\Http\Controllers\Api\TransmissionController;
use App\Http\Controllers\Api\DriverController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'partner'], function () {
    Route::get('/', [PartnerController::class, 'index']);
    Route::post('/save', [PartnerController::class, 'save']);
    Route::post('/delete', [PartnerController::class, 'delete']);
});

Route::group(['prefix' => '/car'], function () {
    Route::get('/', [CarController::class, 'index']);
    Route::post('/save', [CarController::class, 'save']);
    Route::post('/delete', [CarController::class, 'delete']);
});

Route::group(['prefix' => '/mark'], function () {
    Route::get('/', [MarkController::class, 'index']);
    Route::post('/save', [MarkController::class, 'save']);
    Route::post('/delete', [MarkController::class, 'delete']);
    Route::post('/all', [MarkController::class, 'all']);
});

Route::group(['prefix' => '/model-car'], function () {
    Route::get('/', [ModelCarController::class, 'index']);
    Route::post('/save', [ModelCarController::class, 'save']);
    Route::post('/delete', [ModelCarController::class, 'delete']);
    Route::post('/all', [ModelCarController::class, 'all']);
});

Route::group(['prefix' => '/fuel'], function () {
    Route::get('/', [FuelController::class, 'index']);
    Route::post('/save', [FuelController::class, 'save']);
    Route::post('/delete', [FuelController::class, 'delete']);
    Route::post('/all', [FuelController::class, 'all']);
});

Route::group(['prefix' => '/transmission'], function () {
    Route::get('/', [TransmissionController::class, 'index']);
    Route::post('/save', [TransmissionController::class, 'save']);
    Route::post('/delete', [TransmissionController::class, 'delete']);
    Route::post('/all', [TransmissionController::class, 'all']);
});

Route::group(['prefix' => '/driver'], function () {
    Route::get('/', [DriverController::class, 'index']);
    Route::post('/save', [DriverController::class, 'save']);
    Route::post('/delete', [DriverController::class, 'delete']);
    Route::get('/all', [DriverController::class, 'all']);
    Route::get('/{field}/{value}', [DriverController::class, 'ruller'])->where([
        'field' => '.*',
        'value' => '.*'
    ]);
});

//Route::group(['prefix' => '/register'], function () {
////    Route::get('/', [DriverController::class, 'index']);
//    Route::post('/save', [DriverController::class, 'save']);
////    Route::post('/delete', [DriverController::class, 'delete']);
////    Route::get('/all', [DriverController::class, 'all']);
//});