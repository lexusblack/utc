<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Driver extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_name',
        'secondary_name',
        'phone',
        'bill',
        'email',
        'password',
        'front_drive_card',
        'back_drive_card',
        'certificate',
        'passport',
        'photo',
        'car_id'
    ];

    const DISK = 'drivers';

    public $field_files = ['front_drive_card','back_drive_card','certificate','passport','photo'];

    public function car(string $status = ''): BelongsTo
    {
        return $this->belongsTo(Car::class)->when(!empty($status), function ($query) use ($status) {
            /** @var $query Builder */
            $query->where('status', '=', $status);
        });
    }
}
