<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'mark_id',
        'model_id',
        'year',
        'color',
        'number_car',
        'fuel_id',
        'transmission_id',
        'price',
        'date_technical_inspection',
        'technical_inspection_first',
        'technical_inspection_second',
        'date_insurance',
        'insurance_photo',
        'partner_id',
        'status'
    ];

    const DISK = 'cars';

    const STATUS_READY = ['label' => 'Свободна', 'value' => 'ready'];
    const STATUS_BUSY = ['label' => 'Занята', 'value' => 'busy'];
    const STATUS_DAMAGED = ['label' => 'В ремонте', 'value' => 'damaged'];

    public $field_files = ['technical_inspection_first','technical_inspection_second','insurance_photo'];

    public function mark(): BelongsTo
    {
        return $this->belongsTo(Mark::class);
    }

    public function model(): BelongsTo
    {
        return $this->belongsTo(ModelCar::class);
    }

    public function fuel(): BelongsTo
    {
        return $this->belongsTo(Fuel::class);
    }

    public function transmission(): BelongsTo
    {
        return $this->belongsTo(Transmission::class);
    }

    public function partner(): BelongsTo
    {
        return $this->belongsTo(Partner::class);
    }
}
