<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ModelCar extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'mark_id'];

    public function mark(): BelongsTo
    {
        return $this->belongsTo(Mark::class);
    }
}
