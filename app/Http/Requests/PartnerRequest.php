<?php

namespace App\Http\Requests;

use App\Models\Partner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $model = Partner::class;
        $id = '';
        if ($this->get('id', false)) {
            $item = $model::find($this->get('id'));
            $id = $item->id;
        }
        $rules = [
            'name' => 'required',
            'contact' => 'required',
            'email' => ['required', Rule::unique((new $model)->getTable())->ignore($id)],
            'phone' => ['required', Rule::unique((new $model)->getTable())->ignore($id)],
            'password' => 'required'
        ];

        if ($this->has('ids')) {
            $rules = [
                'ids' => 'required'
            ];
        }

        return $rules;
    }
}
