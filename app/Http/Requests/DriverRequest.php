<?php

namespace App\Http\Requests;

use App\Models\Driver;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $model = Driver::class;
        $id = '';
        if ($this->get('id', false)) {
            $item = $model::find($this->get('id'));
            $id = $item->id;
        }
        $rules = [
            'first_name' => 'required',
            'secondary_name' => 'required',
            'phone' => ['required', Rule::unique((new $model)->getTable())->ignore($id)],
            'bill' => ['required', Rule::unique((new $model)->getTable())->ignore($id)],
            'email' => ['required', Rule::unique((new $model)->getTable())->ignore($id)],
            'password' => 'required',
            'front_drive_card' => $this->hasFile('front_drive_card') ? 'image' : '',
            'back_drive_card' => $this->hasFile('back_drive_card') ? 'image' : '',
            'certificate' => $this->hasFile('certificate') ? 'image' : '',
            'passport' => $this->hasFile('passport') ? 'image' : '',
            'photo' => $this->hasFile('photo') ? 'image' : ''
        ];

        if ($this->has('ids')) {
            $rules = [
                'ids' => 'required'
            ];
        }

        return $rules;
    }
}
