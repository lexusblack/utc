<?php

namespace App\Http\Requests;

use App\Models\Car;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $model = Car::class;
        $id = '';
        if ($this->get('id', false)) {
            $item = $model::find($this->get('id'));
            $id = $item->id;
        }
        $rules = [
            'model_id' => 'required',
            'mark_id' => 'required',
            'year' => 'required',
            'color' => 'required',
            'number_car' => ['required', Rule::unique((new $model)->getTable())->ignore($id)],
            'fuel_id' => 'required',
            'transmission_id' => 'required',
            'price' => 'required|integer|min:0',
            'date_technical_inspection' => 'required',
            'date_insurance' => 'required',
            'technical_inspection_first' => $this->hasFile('technical_inspection_first') ? 'image' : '',
            'technical_inspection_second' => $this->hasFile('technical_inspection_second') ? 'image' : '',
            'insurance_photo' => $this->hasFile('insurance_photo') ? 'image' : '',
        ];

        if ($this->has('ids')) {
            $rules = [
                'ids' => 'required'
            ];
        }

        return $rules;
    }
}
