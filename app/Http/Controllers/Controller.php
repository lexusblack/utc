<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param string $message
     * @param array $data
     * @param int $cod
     * @return JsonResponse
     */
    public function success($message = '', $data = [], $cod = 201) : JsonResponse
    {
        $response = array_merge(['message' => $message], $data);
        return response()->json($response, $cod);
    }

    /**
     * @param string $message
     * @param array $data
     * @param int $cod
     * @return JsonResponse
     */
    public function error($message = '', $data = [], $cod = 401) : JsonResponse
    {
        $response = array_merge(['message' => $message], $data);
        return response()->json($response, $cod);
    }
}
