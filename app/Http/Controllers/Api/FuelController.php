<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Fuel;
use Illuminate\Http\Request;
use App\Traits\Catalog;
use App\Http\Requests\FuelRequest;

class FuelController extends Controller
{

    use Catalog;

    protected static $model = Fuel::class;

    public function index(Request $request)
    {
        return $this->indexCatalog($request);
    }

    public function save(FuelRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(FuelRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
