<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\Catalog;
use Illuminate\Http\Request;
use App\Models\Transmission;
use App\Http\Requests\TransmissionRequest;

class TransmissionController extends Controller
{
    use Catalog;

    protected static $model = Transmission::class;

    public function index(Request $request)
    {
        return $this->indexCatalog($request);
    }

    public function save(TransmissionRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(TransmissionRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
