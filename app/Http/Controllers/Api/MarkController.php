<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MarkRequest;
use App\Models\Mark;
use Illuminate\Http\Request;
use App\Traits\Catalog;

class MarkController extends Controller
{
    use Catalog;

    protected static $model = Mark::class;

    public function index(Request $request)
    {
        return $this->indexCatalog($request);
    }

    public function save(MarkRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(MarkRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
