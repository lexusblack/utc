<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ModelCarRequest;
use App\Models\ModelCar;
use Illuminate\Http\Request;
use App\Traits\Catalog;

class ModelCarController extends Controller
{
    use Catalog;

    protected static $model = ModelCar::class;

    public function index(Request $request)
    {
        return $this->indexCatalog($request,true);
    }

    public function save(ModelCarRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(ModelCarRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
