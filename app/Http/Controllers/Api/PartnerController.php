<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Partner;
use App\Traits\Catalog;
use App\Http\Requests\PartnerRequest;

class PartnerController extends Controller
{
    use Catalog;

    protected static $model = Partner::class;

//    protected static $disk = Driver::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request);
    }

    public function save(PartnerRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(PartnerRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
