<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Car;
use App\Http\Requests\CarRequest;
use App\Traits\Catalog;

class CarController extends Controller
{
    use Catalog;

    protected static $model = Car::class;

    protected static $disk = Car::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request, true);
    }

    public function save(CarRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(CarRequest $request)
    {
        return $this->deleteCatalog($request);
    }

}
