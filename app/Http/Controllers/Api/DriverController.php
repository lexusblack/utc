<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\Catalog;
use Illuminate\Http\Request;
use App\Models\Driver;
use App\Http\Requests\DriverRequest;

class DriverController extends Controller
{
    use Catalog;

    protected static $model = Driver::class;

    protected static $disk = Driver::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request, true);
    }

    public function save(DriverRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(DriverRequest $request)
    {
        return $this->deleteCatalog($request);
    }

    public function ruller($field, $value)
    {
        return static::$model::where($field, '=', $value)->count();
    }
}
