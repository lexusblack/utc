<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $results = $this->resource->toArray();

        $results['mark'] = $this->mark;
        $results['model'] = $this->model;
        $results['fuel'] = $this->fuel;
        $results['transmission'] = $this->transmission;

        foreach ($this->resource->field_files as $field) {
            if (empty($this->{$field})) {
                $results[$field] = '';
            } else {
                $results[$field] = Storage::disk('cars')->url($this->{$field});
            }
        }

        return $results;
    }
}
