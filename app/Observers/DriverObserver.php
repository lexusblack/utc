<?php

namespace App\Observers;

use App\Models\Driver;
use Illuminate\Support\Facades\Storage;

class DriverObserver
{
    /**
     * Handle the Driver "created" event.
     *
     * @param  \App\Models\Driver  $driver
     * @return void
     */
    public function created(Driver $driver)
    {
        //
    }

    /**
     * Handle the Driver "updated" event.
     *
     * @param  \App\Models\Driver  $driver
     * @return void
     */
    public function updated(Driver $driver)
    {
        //
    }

    /**
     * Handle the Driver "deleted" event.
     *
     * @param  \App\Models\Driver  $driver
     * @return void
     */
    public function deleted(Driver $driver)
    {
        Storage::disk($driver::DISK)->deleteDirectory($driver->id);
    }

    /**
     * Handle the Driver "restored" event.
     *
     * @param  \App\Models\Driver  $driver
     * @return void
     */
    public function restored(Driver $driver)
    {
        //
    }

    /**
     * Handle the Driver "force deleted" event.
     *
     * @param  \App\Models\Driver  $driver
     * @return void
     */
    public function forceDeleted(Driver $driver)
    {
        //
    }
}
